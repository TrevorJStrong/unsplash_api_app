import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { Home } from '../screens/Tabs/Home/home';
import { ViewImage } from '../screens/View/photo';
import { LikedPhotos } from '../screens/Tabs/LikedPhotos/view'

const {Navigator, Screen} = createStackNavigator();

const Tab = createMaterialTopTabNavigator();

const HomeNavigator = () => (
  <Navigator headerMode={"none"}>
    <Screen 
      name="Home" 
      component={Home} 
    />
    <Screen 
      name="ViewPhoto" 
      component={ViewImage} 
    />
  </Navigator>
);

const TabNavigator = () => (
  <Tab.Navigator style={{ marginTop: Platform.OS === 'android' ? 45 : 0 }}>
    <Tab.Screen 
      name="Home" 
      component={HomeNavigator} 
    />
    <Tab.Screen 
      name="LikedPhotos" 
      component={LikedPhotos}
      options={{ title: 'LIKED PHOTOS '}}
    />
  </Tab.Navigator>
);

export const AppNavigator = () => (
  <NavigationContainer>
    <TabNavigator />
  </NavigationContainer>
);
