import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  backButton: {
      padding: 10
  },
  main_photo: {
    width: '100%', 
    height: 250
  },
  profileImageContainer: {
      padding: 10,
      flexDirection: 'row',
      alignItems: 'center'
  },
  userProfileImage: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2
  },
  username: {
      marginLeft: 15,
      fontSize: 20,
      color: '#464646'
  },
  photoDescription: {
    padding: 10, 
    fontSize: 18,
    color: '#464646'
  }
});