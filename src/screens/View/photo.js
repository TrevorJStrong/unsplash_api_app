import React, {useEffect, useState} from 'react';
import { Image, Text, View, SafeAreaView, ImageBackground, TouchableOpacity } from 'react-native';
import { Entypo } from '@expo/vector-icons'; 
import styles from './styles';

export const ViewImage = ({ navigation: { goBack }, route }) => {

    // fetch photo object that has been passed 
    const { photo } = route.params;

    return (
        <SafeAreaView style={styles.container}>

            <TouchableOpacity onPress={() => goBack()} style={styles.backButton}>
                <Entypo name="chevron-left" size={35} color="#464646" />
            </TouchableOpacity>

            <View style={styles.profileImageContainer}>
                <Image
                    source={{ uri: photo.user.profile_image.medium }}
                    style={styles.userProfileImage}
                />
                <Text style={styles.username}>{photo.user.instagram_username}</Text>
            </View>

            <ImageBackground 
                source={{ uri: photo.urls.full }}
                style={styles.main_photo}
            />

            <Text style={styles.photoDescription}>{photo.alt_description}</Text>

        </SafeAreaView>
    );

}