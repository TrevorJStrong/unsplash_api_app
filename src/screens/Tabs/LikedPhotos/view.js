import React, {useEffect, useState} from 'react';
import { FlatList, Text, View, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import styles from './styles';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const LikedPhotos = ({ navigation, route }) => {

    const [ items, setItem ] = useState([]);

    useEffect(() => {
        // add navigation focus listener so when a user likes a photo
        const add_listener = navigation.addListener('focus', () => {
            getLikedPhotos();
        });

    }, [])

    // fetch liked photos from device using AsysnStorage
    const getLikedPhotos = async () => {
        try {
            const likedPhotos = await AsyncStorage.getItem('LikedPhotos');
            const likedPhotoObject = JSON.parse(likedPhotos);
            setItem(likedPhotoObject); 
        } catch (error) {
            console.log(error);
        }
    }

    // render the users liked photos
    const renderItems = (item, index) => {
        return (
            <TouchableOpacity onPress={() => navigation.navigate("ViewPhoto", {
                photo: item
            })}>
                <ImageBackground 
                    source={{ uri: item.urls.full }}
                    style={styles.photos}
                >
                    <View style={styles.likeIcon}>
                        <AntDesign name="heart" size={30} color="#DD2A7B" />
                    </View>

                    <View style={styles.photoInfo}>
                        <AntDesign name="heart" size={24} color="#DD2A7B" />
                        <Text style={styles.totalLikes}>
                            <Text style={styles.totalLikesBold}>{item.likes} </Text>
                            likes
                        </Text>
                    </View>

                </ImageBackground>
            </TouchableOpacity>
        )
    }

    // render if the items array is empty
    const renderEmptyMessage = () => {
        return (
            <View style={styles.isEmptyContainer}>
                <Text style={styles.isEmptyMessage}>Your liked photos will appear here!</Text>
                <AntDesign name="heart" size={80} color="#787878" />
            </View>
        );
    }

    return (
        <SafeAreaView style={styles.container}>

            <FlatList 
                data={items}
                keyExtractor={item => item.id}
                renderItem={({ item, index }) => (renderItems(item, index))}
                ListEmptyComponent={() => renderEmptyMessage()}
            />


        </SafeAreaView>
    );

}