import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  loadingIndicator: {
    marginTop: 30
  },
  photos: {
    width: '100%', 
    height: 250
  },
  likeIcon: {
    marginTop: 15,
    marginRight: 15,
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    width: 50,
    height: 50,
    borderRadius: 50 / 2
  },
  photoInfo: {
    position: 'absolute', 
    bottom: 0, 
    padding: 10,
    backgroundColor: '#d6d6d6', 
    width: '100%', 
    opacity: 0.8, 
    flexDirection: 'row',
    alignItems: 'center'
  },
  totalLikes: {
    marginLeft: 10,
    color: '#464646'
  },
  totalLikesBold: {
    fontWeight: 'bold',
    color: '#464646'
  },
  isEmptyContainer: {
    alignItems: 'center',
    marginTop: 50
  },
  isEmptyMessage: {
    fontSize: 22,
    marginBottom: 40
  },
});