import React, {useEffect, useState} from 'react';
import { FlatList, Text, View, SafeAreaView, ImageBackground, TouchableOpacity, ActivityIndicator } from 'react-native';
import Unsplash, { toJson } from 'unsplash-js';
import styles from './styles';
import { AntDesign } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';

const APP_ACCESS_KEY = 'xWjcIFrpL4z-HtOvS5Ca1UN2buTshaph97F20lndaj8';

const unsplash = new Unsplash({ 
    accessKey: APP_ACCESS_KEY, 
    secret: "5rswa7Jm58mrvNstvylNaxO7QMrhf6NgkmF6OXS4FaM",
    callbackUrl: 'urn:ietf:wg:oauth:2.0:oob'
});

export const Home = ({ navigation }) => {

    // create state
    const [ items, setItems ] = useState([]);
    const [ data, setData ] = useState([]);
    const [ liked, setLiked ] = useState(false);
    const [ loading, setLoading ] = useState(true);

    // call functions once component has mounted
    useEffect(() => {
        // fetch photos
        getImages();
        // request access token
        requestAccessToken();
    }, []);

    // Request an access token 
    const requestAccessToken = () => {
        try {
            unsplash.auth.userAuthentication('PbM5NwFX_7D4JAkOekLxZKChOyuDTrx5Q9JJfCsjRnE')
            .then(toJson)
            .then(json => {
                unsplash.auth.setBearerToken(json.access_token);
            }) 
        } catch (error) {
            console.log(error);
        }
    }   

    // fetch images once component is mounted 
    const getImages = () => {
        try {
            unsplash.photos.listPhotos(1, 3, "latest")
            .then(toJson)
            .then(json => {
                console.log(json);
                // pass json response to the items array
                setItems(json);
                // set loading to false once the response is successfully
                setLoading(false);
        });
        } catch (error) {
            console.log(error);
        }
    }

    // like photo
    const likePhoto = (item, index) => {
        unsplash.photos.likePhoto(item.id)
        .then(toJson)
        .then(json => {
            if(item.isActive == true) {
                unsplash.photos.unlikePhoto(item.id);
            } else {
                let tmp = items;
                // retrive selected index
                tmp[index].isActive = !tmp[index].isActive;
                // add selected index to items array
                setItems(tmp);
                // call savePhoto to store image
                savePhoto(item);
            }
        });
    }

    // save selected photo onto device
    const savePhoto = async(item) => {

        // get item object 
        let photo = item;

        // add photo object to new array
        const liked_photos_array = [item];

        // fetch currently stored photos on device
        const stored_photos = await AsyncStorage.getItem('LikedPhotos');

        // parse JSON string to an object
        const stored_photos_parsed = JSON.parse(stored_photos);
        setData(stored_photos_parsed);

        // create new array which will be used to update the LikedPhotos object
        let updated_photos = [];

        // check if the user hasnt liked any photos yet
        // if not, then call setItem to save the photo object
        if(stored_photos === null) {
            // save photo object and store on device using AsyncStorage (setItem)
            await AsyncStorage.setItem('LikedPhotos', JSON.stringify(liked_photos_array));
        } else {
            // save and update the updated_photos with the previous data + with the new photo object 
            updated_photos = [...stored_photos_parsed, photo];
            await AsyncStorage.setItem('LikedPhotos', JSON.stringify(updated_photos));
        }
    }

    // render the photos
    const renderItems = (item, index) => {
        return (
            <TouchableOpacity onPress={() => navigation.navigate("ViewPhoto", {
                photo: item
            })}>
                <ImageBackground 
                    source={{ uri: item.urls.full }}
                    style={styles.photos}
                >
                    <TouchableOpacity onPress={() => likePhoto(item, index)} style={styles.likeIcon}>
                        <AntDesign name="heart" size={30} color={item.isActive == true ? "#DD2A7B" : '#464646'} />
                    </TouchableOpacity>

                    <View style={styles.photoInfo}>
                        <AntDesign name="heart" size={24} color="#DD2A7B" />
                        <Text style={styles.totalLikes}>
                            <Text style={styles.totalLikesBold}>{item.likes} </Text>
                            likes
                        </Text>
                    </View>

                </ImageBackground>
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            
            {loading == true &&
                <ActivityIndicator
                    color="#464646"
                    size="small"
                    style={styles.loadingIndicator}
                />
            }
            
            <FlatList 
                data={items}
                //keyExtractor={item => item.id}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => renderItems(item, index)}
            />

        </SafeAreaView>
    );

}