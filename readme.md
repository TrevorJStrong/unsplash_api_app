# Unplash Gram App

This project allows the user to view a list of images using the Unsplash API. The user can also like an image which is stored within
its own 'LIKED PHOTOS' tab. The user can also see information about an image, such as the person who uploaded it, the amount of likes and the description.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

The only Prerequisites needed is to have expo-cli installed along with node.js (LTS release or later) and git.

### Installing

Running npm install should be all that is needed for you to get the development running once you clone the project (git clone https://TrevorJStrong@bitbucket.org/TrevorJStrong/unsplash_api_app.git).

Once you run npm install, it should show the list npm packages that was used for this project, which were the following:

* [React-Navgiation5](https://reactnavigation.org/docs/getting-started) - The navigation system used.
* [Async-Storage](https://react-native-async-storage.github.io/async-storage/docs/install/) - Storage system used.

## Built With

* [Expo](https://docs.expo.io/) - The framework used
* [NPM](https://www.npmjs.com/) - Dependency Management

## Authors

* **Trevor Strong**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details