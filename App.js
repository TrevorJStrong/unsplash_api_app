import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { SafeAreaView } from 'react-native';
import { AppNavigator } from './src/components/navigation';

export default function App() {

  useEffect(() => {
    
  }, []);

  return (
    <SafeAreaView style={{ flex: 1}}>
      <AppNavigator />
    </SafeAreaView>
  )
}
